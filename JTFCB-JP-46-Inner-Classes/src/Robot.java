// outer klasa
public class Robot {

	private int id;

	// Non-static nested classes have access to the enclosing
	// class's instance data. E.g. implement Iterable
	// http://www.caveofprogramming.com/java/using-iterable-java-collections-framework-video-tutorial-part-11/
	// Use them to group functionality.
	// pierwszy typ - non static inner classes
	// do grupowania funkcjonalności sluza wlasnie inner classes
	// i zeby ta klasa miala dostep do int string tej klasy glownej
	// to jest inner class
	private class Brain {

		public void think() {

			System.out.println("Robot " + id + " is thinking");
		}

	}

	// static inner classes do not have access to instance data.
	// They are really just like "normal" classes, except that they are grouped
	// within an outer class. Use them for grouping classes together.
	// drugi static inner class
	// sluza do zrobienia nowej klasy ktora nie jest skojarzona z inna inner
	// klasa
	// ale chcesz ja powiazac z outer klassa czyli tu Robot
	public static class Battery {

		public void charge() {

			System.out.println("Battery charging...");

		}

	}

	public Robot(int id) {
		this.id = id;
	}

	public void start() {

		System.out.println("Starting robot " + id);

		// Use Brain. We don't have an instance of brain
		// until we create one. Instances of brain are
		// always associated with instances of Robot (the
		// enclosing class).
		Brain brain = new Brain();
		brain.think();

		// musi byc final
		final String name = "Robert";

		// Sometimes it's useful to create local classes
		// within methods. You can use them only within the method.
		// przyklad 3 gdzie mozna zrobic klase wewnatrz metody
		// zeby miec dostep do int, string ale musi byc final
		class Temp {

			public void doSomething() {

				System.out.println("ID is: " + id);
				System.out.println("My name is " + name);
			}

		}

		Temp temp = new Temp();
		temp.doSomething();

	}

}
